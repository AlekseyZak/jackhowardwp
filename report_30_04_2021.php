Report 30.04.2021:

@Zarina @Nathan Today I worked on:

- installed the "Listo" plugin to use a country list for the "Topic" dropdown;
- installed the "Drag and Drop Multiple File Upload - Contact Form 7" plugin to apply styles to the file upload area;
- applied code changes in the "Contact Form CFDB7" plugin ('admin-form-details.php').
These changes are needed in order to correctly display and open links to uploaded photos and websites (instagram);

all pages / forms have been implemented:
- https://wordpress-585324-1895170.cloudwaysapps.com/send-to-jack/
- https://wordpress-585324-1895170.cloudwaysapps.com/contact-us/
- https://wordpress-585324-1895170.cloudwaysapps.com/become-educator/
- https://wordpress-585324-1895170.cloudwaysapps.com/become-model/
- https://wordpress-585324-1895170.cloudwaysapps.com/coaching/

At the moment, the ability to send Emails after the forms submit has not been tested (most likely they need a little configure).
But all form data (after submit) will be saved in the DB.

Field validations:
- almost all fields required
- "Instagram" and "Website" fields validate as Link
- email validation
- "Upload Photo" validations: max size - 20MB, image filetypes - gif|png|jpg|jpeg, only 1 file can be upload

usage:

1. Go to some pages, e.g. - https://wordpress-585324-1895170.cloudwaysapps.com/send-to-jack/
2. Fill in all the fields (all fields required)
3. Select or drag an image to the area of the photo field
4. Click the "Submit" button. After successfully submitting the form, the following message will appear: "Thank you for your message. It has been sent."
5. Go to the AdminPanel (wp-admin)
6. Open the Contact Forms Menu ( https://wordpress-585324-1895170.cloudwaysapps.com/wp-admin/admin.php?page=cfdb7-list.php )
7. Choose the "Send to Jack" form. Here you can see the list of submitted forms data
8. Click one of the rows. Details of the form data will be shown here
9. You can click on the lines "Instagram" or "Upload a photo" and follow these links (in a separate window)

Check please and let me know your feedback.
Have a great weekend!