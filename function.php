<?php
add_action('after_setup_theme', 'uncode_language_setup');
function uncode_language_setup()
{
    load_child_theme_textdomain('uncode', get_stylesheet_directory() . '/languages');
}

function theme_enqueue_styles()
{
    $production_mode = ot_get_option('_uncode_production');
    $resources_version = ($production_mode === 'on') ? null : rand();
    if ( function_exists('get_rocket_option') && ( get_rocket_option( 'remove_query_strings' ) || get_rocket_option( 'minify_css' ) || get_rocket_option( 'minify_js' ) ) ) {
        $resources_version = null;
    }
    $parent_style = 'uncode-style';
    $child_style = array('uncode-style');
    wp_enqueue_style($parent_style, get_template_directory_uri() . '/library/css/style.css', array(), $resources_version);
    wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css', $child_style, $resources_version);
}
add_action('wp_enqueue_scripts', 'theme_enqueue_styles', 100);

add_filter('wpcf7_autop_or_not', '__return_false');


/*
 * Show all post status
 */
function allow_pending_posts_wpse_103938($qry) {
    $user_id = get_current_user_id();
    $user = new MeprUser( $user_id );
    $active_membership_ids = $user->active_product_subscriptions('ids');
    $titles = array();
    if(!empty($active_membership_ids)) {
        foreach($active_membership_ids as $id) {
            $membership = new MeprProduct($id);
            $titles[] = $membership->post_title;
        }
    }

//var_dump($titles);

    $active = false;
    if(in_array('Unlimited Annual',$titles) || in_array('Unlimited Monthly',$titles)){
        $active = true;
    }
    if (!is_admin()) {
// if (!is_admin() && (current_user_can('edit_posts') || $active )) {
        $qry->set('post_status', array('publish','coming-soon','new'));
        $page_id = get_queried_object_id();
        var_dump($page_id);
    }
}
add_action('pre_get_posts','allow_pending_posts_wpse_103938');


/*
 * Add Classes to post layout
 */
function bcsc2017_post_class($classes) {
    global $post;
    $current_status = get_post_status ( $post->ID );
    $user_id = get_current_user_id();
    $user = new MeprUser( $user_id );
    $active_membership_ids = $user->active_product_subscriptions('ids');
    $titles = array();
    if(!empty($active_membership_ids)) {
        foreach($active_membership_ids as $id) {
            $membership = new MeprProduct($id);
            $titles[] = $membership->post_title;
        }
    }

    $active = false;
    if(in_array('Unlimited Annual',$titles) || in_array('Unlimited Monthly',$titles)){
        $active = true;
    }

    $add = get_user_meta( $user_id, 'postid-'.$post->ID, true );

    $classes[] = 'pss-'. $current_status . ' ' . ($active || current_user_can('edit_posts') ? 'pss-unlimited' : 'pss-pasg') . ' ' . ($add ? 'pss-p' : 'pss-np');
    return $classes;
}
add_filter( 'post_class', 'bcsc2017_post_class' );

function append_query_string( $url, $post, $leavename=false ) {

    $user_id = get_current_user_id();
    $user = new MeprUser( $user_id );
    $active_membership_ids = $user->active_product_subscriptions('ids');
    $titles = array();
    if(!empty($active_membership_ids)) {
        foreach($active_membership_ids as $id) {
            $membership = new MeprProduct($id);
            $titles[] = $membership->post_title;
        }
    }

//var_dump($titles);

    $active = false;
    if(in_array('Unlimited Annual',$titles) || in_array('Unlimited Monthly',$titles)){
        $active = true;
    }

    $current_status = get_post_status ( $post->ID );
    if ( $post->post_type == 'post'  && $current_status != 'publish' ) {
        if((current_user_can('edit_posts') || $active )){
            if($current_status == "pending"){
                $current_status = "new";
            }
            $url = add_query_arg( 'access', $current_status, $url );
        } else {
            $url = add_query_arg( 'access', 'coming-soon', "/online-training" );
        }
    }
    return $url;
}
add_filter( 'post_link', 'append_query_string', 10, 3 );

// define the get_comments_number callback
function filter_get_comments_number( $count, $post_id ) {
    return '';
};

// add the filter
add_filter( 'get_comments_number', 'filter_get_comments_number', 10, 2 );
add_filter( 'get_comments_link', function( $link, $post_id )
{
    $meta = get_post_meta($post_id, 'preview_video', true);
    $hash = $meta ? '?preview' : '/online-training?no-preview=1';
    return $meta . $hash;

}, 10, 2 );

function set_wp_nav_menu_args( $args = '' ) {
    $slug = basename(get_permalink());

    if( is_user_logged_in() ) {
        $args['menu'] = 'Menu - Logged In';
        if ( $slug == 'join-us' || $slug == 'pay-as-you-go' || $slug == 'login' || $slug == 'unlimited-annual' || $slug == 'unlimited-monthly' ) {
            $args['menu'] = 'Menu - Create Account';
        }
    } else {
        if ( $slug == 'join-us' || $slug == 'pay-as-you-go' || $slug == 'login' || $slug == 'unlimited-annual' || $slug == 'unlimited-monthly' ) {
            $args['menu'] = 'Menu - Create Account';
        }
        $args['menu'] = 'Menu - Logged Out';
    }

    return $args;
}
add_filter( 'wp_nav_menu_args', 'set_wp_nav_menu_args' );

/**
 * Enqueue our Scripts and Styles Properly
 */
function theme_enqueue() {

    $theme_url  = get_template_directory_uri();     // Used to keep our Template Directory URL
    $ajax_url   = admin_url( 'admin-ajax.php' );        // Localized AJAX URL

    // Localize Our Script so we can use `ajax_url`
    wp_localize_script(
        'um-modifications',
        'ajax_url',
        $ajax_url
    );

    // Finally enqueue our script
    wp_enqueue_script( 'um-modifications' );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue' );

/**
 * AJAX Callback
 * Always Echos and Exits
 */
function um_modifications_callback() {

    // Ensure we have the data we need to continue
    if( ! isset( $_POST ) || empty( $_POST ) || ! is_user_logged_in() ) {

        // If we don't - return custom error message and exit
        header( 'HTTP/1.1 400 Empty POST Values' );
        echo 'Could Not Verify POST Values.';
        exit;
    }

    $user_id        = get_current_user_id();
    $um_val         = sanitize_text_field( $_POST['postID'] );

    update_user_meta( $user_id, 'postid-'.$um_val, $um_val );
    exit;
}
add_action( 'wp_ajax_nopriv_um_cb', 'um_modifications_callback' );
add_action( 'wp_ajax_um_cb', 'um_modifications_callback' );