<div class="contact-us-form-content">

  <h2>Send to Jack</h2>

  <h4>Send us your details below for the chance to become a Jack Howard model.</h4>

  <hr>

  <div class="cu-fields-wrapper">

    <div>
      <label> First Name
        [text* first-name]
      </label>
    </div>

    <div>
      <label> Email
        [email* email]
      </label>
    </div>

    <div>
      <label> Your Instagram @
        [url* instagram]
      </label>
    </div>

    <div class="--field-upload-photo">
      <label for="upload-photo"> Upload Photo
        [mfile* upload-photo limit:20000000 filetypes:gif|png|jpg|jpeg min-file:1 max-file:1]
      </label>
    </div>

  </div>

  <div class="--submit-btn">
    [submit "Submit"]
  </div>

</div>

<style>

    .contact-us-form-content {
        margin-bottom: 60px;
        padding-right: 15px;
        padding-left: 50px;
    }

    @media (max-width: 1024px) {
        .contact-us-form-content {
            margin-bottom: 0;
            padding-right: 15px;
            padding-left: 15px;
        }
    }

    .contact-us-form-content .--submit-btn {
        text-align: left;
    }

    .contact-us-form-content > p,
    .contact-us-form-content > h4,
    .contact-us-form-content > h2 {
        text-align: left;
    }

    .contact-us-form-content > h4 {
        margin-bottom: 30px;
    }

    /* general styles */

    .codedropz-upload-handler {
        background-color: white;
        border-width: 1px;
        font-size: 0.75em;
        padding: 20px 40px;
        color: #c5c5c5;
    }

    .codedropz-upload-handler h3 {
        color: #c5c5c5;
        font-size: 1.5em;
        text-transform: capitalize;
    }

    .codedropz-upload-handler a {
        color: #c5c5c5;
    }

    .dnd-upload-counter {
        display: none;
    }

    .main-container .row-container .row-parent {
        padding-top: 0;
        padding-right: 0;
        margin-right: 15px;
    }
</style>

<script>
    jQuery('.wpcf7-select').on('change', function () {
        jQuery(this).css('color', 'black');
    });
</script>
<style>
    /* contact forms common styles */
    .cu-fields-wrapper input,
    .cu-fields-wrapper select {
        width: 100%;
        margin-top: 5px;
    }

    .cu-fields-wrapper select {
        color: #95989A;
    }

    .cu-fields-wrapper select option[value=""] {
        display: none;
    }

    .cu-fields-wrapper select option {
        color: #000001;
    }

    .cu-fields-wrapper > div {
        margin-bottom: 25px;
    }

    .cu-fields-wrapper textarea::placeholder {
        color: #95989A;
    }

    /* general styles */
    .wpcf7 .wpcf7-mail-sent-ok,
    .wpcf7 .wpcf7-validation-errors,
    .wpcf7 span.wpcf7-not-valid-tip {
        margin-top: 2px;
        margin-bottom: 10px;
    }

    input.wpcf7-form-control.wpcf7-submit {
        background-color: #94837E;
        margin-top: 25px;
        padding: 18px 45px;
        color: white;
        text-transform: initial;
        border-radius: 3px !important;
    }
</style>