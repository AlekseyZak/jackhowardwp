<div class="contact-us-form-content">

    <h2>Contact Us</h2>

    <h4>Thanks for getting in touch! Please use the contact form below to send your inquiry, and we'll reply as soon as
        possible.</h4>

    <div class="cu-fields-wrapper">

        <div class="--field-first-name">
            <label> First Name
                [text* first-name]
            </label>
        </div>

        <div class="--field-last-name">
            <label> Last Name
                [text* last-name]
            </label>
        </div>

        <div class="--field-email">
            <label> Email
                [email* email]
            </label>
        </div>

        <div class="--field-topic">
            <label> Topic
                [select* topic first_as_label "- Please Select -" "I have some feedback" "I’m looking for some help or support" "I have some billing or subscription
                questions"]
            </label>
        </div>

        <div class="--field-message">
            <label> Message
                [textarea* message]
            </label>
        </div>

    </div>

    <div class="--submit-btn">
        [submit "Submit"]
    </div>

</div>

<style>
    /* page styles */
    .contact-us-form-content {
        margin-bottom: 60px;
    }

    .contact-us-form-content .--submit-btn {
        text-align: center;
    }

    .contact-us-form-content > p,
    .contact-us-form-content > h4,
    .contact-us-form-content > h2 {
        text-align: center;
    }

    .contact-us-form-content > h4 {
        margin-top: 10px;
    }

    .contact-us-form-content > h4 {
        margin: 30px 110px 70px;
    }

    /* page styles */
    .cu-fields-wrapper {
        display: grid;
        grid-gap: 5px 15px;
        grid-template-columns: 50% 50%;
        grid-template-areas:
            'first_name last_name'
            'email topic'
            'message message';
    }

    @media (max-width: 1024px) {
        .cu-fields-wrapper {
            display: block;
        }
        .contact-us-form-content > h4 {
            margin: 30px 30px 50px;
        }
    }

    .cu-fields-wrapper .--field-first-name {
        grid-area: first_name
    }

    .cu-fields-wrapper .--field-last-name {
        grid-area: last_name
    }

    .cu-fields-wrapper .--field-email {
        grid-area: email
    }

    .cu-fields-wrapper .--field-topic {
        grid-area: topic
    }

    .cu-fields-wrapper .--field-message {
        grid-area: message
    }
</style>

<script>
    jQuery('.wpcf7-select').on('change', function () {
        jQuery(this).css('color', 'black');
    });
</script>
<style>
    /* contact forms common styles */
    .cu-fields-wrapper input,
    .cu-fields-wrapper select {
        width: 100%;
        margin-top: 5px;
    }

    .cu-fields-wrapper select {
        color: #95989A;
    }

    .cu-fields-wrapper select option[value=""] {
        display: none;
    }

    .cu-fields-wrapper select option {
        color: #000001;
    }

    .cu-fields-wrapper > div {
        margin-bottom: 25px;
    }

    .cu-fields-wrapper textarea::placeholder {
        color: #95989A;
    }

    /* general styles */
    .wpcf7 .wpcf7-mail-sent-ok,
    .wpcf7 .wpcf7-validation-errors,
    .wpcf7 span.wpcf7-not-valid-tip {
        margin-top: 2px;
        margin-bottom: 10px;
    }

    input.wpcf7-form-control.wpcf7-submit {
        background-color: #94837E;
        margin-top: 25px;
        padding: 18px 45px;
        color: white;
        text-transform: initial;
        border-radius: 3px !important;
    }
</style>